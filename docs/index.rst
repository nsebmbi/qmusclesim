.. uEMG squared documentation master file, created by
    sphinx-quickstart on Fri Mar  4 14:07:51 2016.
    You can adapt this file completely to your liking, but it should at least
    contain the root `toctree` directive.

Welcome to uEMG squared's documentation!
========================================

Contents:

.. toctree::
           :maxdepth: 3

           intro.rst
           install.rst
           api/uemg_squared.rst
           tutorial.rst
           refs.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
