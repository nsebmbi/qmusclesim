__author__ = 'DanielSuarez'

# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Thu Jun 13 11:25:50 2013
# by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui


class Ui_Dialog(object):
    def setupUiadd(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(230, 204)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(37, 160, 156, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(30, 20, 171, 126))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label_Nom = QtGui.QLabel(self.formLayoutWidget)
        self.label_Nom.setObjectName("label_Nom")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_Nom)
        self.comboBox_Nom = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_Nom.setObjectName("comboBox_Nom")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBox_Nom)
        self.label_Type = QtGui.QLabel(self.formLayoutWidget)
        self.label_Type.setObjectName("label_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_Type)
        self.comboBox_Type = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_Type.setObjectName("comboBox_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBox_Type)
        self.label_P1 = QtGui.QLabel(self.formLayoutWidget)
        self.label_P1.setObjectName("label_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_P1)
        self.lineEdit_P1 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_P1.setInputMethodHints(QtCore.Qt.ImhFormattedNumbersOnly)
        self.lineEdit_P1.setObjectName("lineEdit_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEdit_P1)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.add_param)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def setupUimod(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(230, 204)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(37, 160, 156, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(30, 20, 171, 126))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label_Nom = QtGui.QLabel(self.formLayoutWidget)
        self.label_Nom.setObjectName("label_Nom")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_Nom)
        self.label_Type = QtGui.QLabel(self.formLayoutWidget)
        self.label_Type.setObjectName("label_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_Type)
        self.comboBox_Type = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_Type.setObjectName("comboBox_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBox_Type)
        self.label_P1 = QtGui.QLabel(self.formLayoutWidget)
        self.label_P1.setObjectName("label_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_P1)
        self.lineEdit_P1 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_P1.setInputMethodHints(QtCore.Qt.ImhFormattedNumbersOnly)
        self.lineEdit_P1.setObjectName("lineEdit_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEdit_P1)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.add_param)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Parameters", None,
                                                           QtGui.QApplication.UnicodeUTF8))
        self.label_Nom.setText(QtGui.QApplication.translate("Dialog", "Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_Type.setText(QtGui.QApplication.translate("Dialog", "Unit", None, QtGui.QApplication.UnicodeUTF8))
        self.label_P1.setText(QtGui.QApplication.translate("Dialog", "Value", None, QtGui.QApplication.UnicodeUTF8))
