# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
import sys

import h5py
import matplotlib
import matplotlib.pyplot
import numpy
import pyqtgraph as pg
from PyQt4 import QtGui, QtCore
from pkg_resources import resource_filename
from pyqtgraph import exporters

matplotlib.pyplot.style.use('bmh')


class EMGExplorer(QtGui.QMainWindow):
    def __init__(self):
        super(EMGExplorer, self).__init__()
        self.filename = None
        self.graph = None
        self.grids = []
        self.coords = []
        self.surf_attrs = dict()
        self.surfs = dict()

        self.utimer = QtCore.QTimer(self)
        self.utimer.timeout.connect(self.forward)

        exitAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/exit.png')),
                                   'Exit', self)
        exitAction.setShortcut('Ctrl+Q')
        exitAction.setToolTip('Exit application')
        exitAction.triggered.connect(self.close)

        loadAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/open.png')),
                                   'Load', self)
        loadAction.setShortcut('Ctrl+O')
        loadAction.setToolTip('load application')
        loadAction.triggered.connect(self.load)

        self.minimapAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/minimap.png')), 'Minimap', self)
        self.minimapAction.setShortcut('Ctrl+M')
        self.minimapAction.setToolTip('Open Minimap')
        self.minimapAction.triggered.connect(self.minimap)
        self.minimapAction.setEnabled(False)

        self.playAction = QtGui.QAction(QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/play.png')),
                                        'Play', self)
        self.playAction.setToolTip("Play simulation")
        self.playAction.triggered.connect(self.playsimu)
        self.playAction.setEnabled(False)

        self.pauseAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/pause.png')), 'Pause', self)
        self.pauseAction.setToolTip("Pause simulation")
        self.pauseAction.triggered.connect(self.pausesimu)
        self.pauseAction.setEnabled(False)

        self.photoAction = QtGui.QAction(
            QtGui.QIcon(resource_filename(self.__module__.split('.')[0], 'icons/photo.png')), 'ScreenShot', self)
        self.photoAction.setToolTip("Screenshot simulation")
        self.photoAction.triggered.connect(self.screenshot)
        self.photoAction.setEnabled(False)

        aboutAction = QtGui.QAction('About', self)
        aboutAction.setToolTip("About qtviewer")
        aboutAction.triggered.connect(self.about)

        menubar = self.menuBar()
        menubar.setNativeMenuBar(False)
        fileMenu = menubar.addMenu('&File')
        fileMenu.addAction(loadAction)
        fileMenu.addAction(exitAction)
        menubar.addAction(aboutAction)

        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(loadAction)
        toolbar.addSeparator()
        toolbar.addWidget(QtGui.QLabel("Surface:"))
        self.combo_surf = QtGui.QComboBox()
        self.combo_surf.currentIndexChanged.connect(self.plot)
        self.combo_surf.setEnabled(False)
        toolbar.addWidget(self.combo_surf)
        toolbar.addSeparator()
        toolbar.addWidget(QtGui.QLabel("Grid:"))
        self.combo_grid = QtGui.QComboBox()
        self.combo_grid.currentIndexChanged.connect(self.plot)
        self.combo_grid.setEnabled(False)
        toolbar.addWidget(self.combo_grid)
        toolbar.addSeparator()
        toolbar.addWidget(QtGui.QLabel("Electrode:"))
        self.spin_elec = QtGui.QSpinBox(self)
        toolbar.addWidget(self.spin_elec)
        self.spin_elec.valueChanged.connect(self.update)
        self.spin_elec.setEnabled(False)
        toolbar.addSeparator()
        toolbar.addAction(self.minimapAction)

        self.time_slider = QtGui.QSlider(QtCore.Qt.Horizontal)
        self.time_slider.setTickPosition(2)
        self.time_slider.setEnabled(False)
        self.time_slider.valueChanged[int].connect(self.updateTime)
        self.time_label = QtGui.QLabel("0.00 s ")

        toolbar.addSeparator()
        toolbar.addAction(self.playAction)
        toolbar.addAction(self.pauseAction)
        toolbar.addWidget(self.time_slider)
        toolbar.addWidget(self.time_label)
        toolbar.addSeparator()
        toolbar.addAction(self.photoAction)

        self.setWindowTitle('EMG QtExplorer v0.1')
        self.figmap = None
        self.cmap = matplotlib.pyplot.get_cmap("viridis")

    def about(self):
        QtGui.QMessageBox.about(None, "About QtEMGViewer", "Simulated EMG Signals viewer.")


    def playsimu(self):
        self.utimer.stop()
        self.utimer.start(100)


    def pausesimu(self):
        self.utimer.stop()


    def forward(self):
        val = self.time_slider.value()
        if val >= self.time_slider.maximum():
            self.utimer.stop()
            self.time_slider.setValue(0)
        else:
            self.time_slider.setValue(val + 1)

    def updateTime(self, value):
        self.time_label.setText("{:3.2f} s".format(self.time[value]))
        self.update()

    def screenshot(self, filename=None):
        if not filename:
            filename = str(QtGui.QFileDialog.getSaveFileName(self, "Save file", "", "PNG (*.png);; SVG (*.svg)"))
        exporters.ImageExporter(self.graph.scene()).export(filename)

    def load(self):
        self.grids = []
        self.coords = []
        self.surfs = dict()
        self.combo_surf.clear()
        self.combo_grid.clear()
        fname = str(QtGui.QFileDialog.getOpenFileName(self, 'Open HDF5 file',
                                                      '', "HDF5 (*.hdf5)"))
        if fname:
            self.workdir, self.filename = os.path.split(fname)
            with h5py.File(fname, "r") as f:
                self.time = numpy.array(f["/time"])*1e-3

                if "myometrium" in f.keys():
                    if len(f['myometrium'].attrs["shape"])==2:
                        if "Vm" in f['myometrium'].keys():
                            self.surfs["Myom. Potential"]=numpy.array(f['myometrium']["Vm"])
                        if "Force" in f['myometrium'].keys():
                            self.surfs["Myom. Force"] = numpy.array(f['myometrium']["Force"])
                        if "Stretches" in f['myometrium'].keys():
                            self.surfs["Myom. Stretch"] = numpy.array(f['myometrium']["Stretches"])

                if "Skin" in f.keys():
                    if "Potential" in f['Skin'].keys():
                        self.surfs["Skin Potential"] = numpy.array(f['Skin']['Potential'])

                if "EMG" in f.keys():
                    grid_names = [group for group in f["/EMG"].keys() if group.startswith("Grid")]
                    for name in grid_names:
                        grid = dict(f["/EMG"][name].attrs)
                        grid["id"] = name.split("_")[-1]
                        grid["signals"] = numpy.array(f["/EMG"][name]["Monopolar"])
                        self.grids.append(grid)
                        self.coords.append(f["/EMG"][name].attrs["centers"])

        if not self.grids or not self.surfs:
            QtGui.QMessageBox.warning(self,"Error while loading HDF5","No grids or no surfaces to display in the file: {}.\n Please open another file.".format(fname))
        else:
            self.nbgrids = len(self.grids)
            for id in range(self.nbgrids):
                self.combo_grid.addItem(str(id))

            for name in self.surfs.keys():
                self.surfs[name] -= self.surfs[name].min()
                self.surfs[name] *= 1.0/self.surfs[name].max()
                self.combo_surf.addItem(name)

            if self.surfs.keys():
                self.combo_surf.setEnabled(True)
            if self.grids:
                self.combo_grid.setEnabled(True)
                self.spin_elec.setMaximum(self.grids[0]["signals"].shape[0]-1)
                self.spin_elec.setEnabled(True)
            self.minimapAction.setEnabled(True)
            self.time_slider.setRange(1, self.time.shape[0] - 1)
            self.time_slider.setEnabled(True)
            self.playAction.setEnabled(True)
            self.pauseAction.setEnabled(True)
            self.photoAction.setEnabled(True)



    def plot(self):
        try:
            self.utimer.stop()
            self.time_slider.setValue(0)
            pg.setConfigOption('background', (238, 238, 238))
            pg.setConfigOption('foreground', 'k')
            grid_id = self.combo_grid.currentIndex()
            surf_name = self.combo_surf.currentText()
            self.spin_elec.setValue(0)
            self.spin_elec.setMaximum(self.grids[grid_id]["signals"].shape[0]-1)
            self.signal = self.grids[grid_id]["signals"][self.spin_elec.value()]

            self.image = self.surfs[str(surf_name)]

            self.graph = pg.GraphicsLayoutWidget(border = pg.mkPen((0, 0, 0)))
            pen = pg.mkPen(color=(52, 138, 189), width=2)
            self.setCentralWidget(self.graph)

            self.imgitem = pg.ImageItem(self.cmap(self.image[1]))
            self.viewbox = self.graph.addPlot(lockAspect=True, row=0, col=0, rowspan=2, colspan=1)  #
            self.viewbox.addItem(self.imgitem)
            self.timeplot = self.graph.addPlot(row=0, col=1, rowspan=1, colspan=1)
            self.fftplot = self.graph.addPlot(row=1, col=1, rowspan=1, colspan=1)
            self.viewbox.setMouseEnabled(x=False, y=False)

            self.timeplot.plot(x=self.time, y=self.signal, pen=pen)
            self.timeplot.setMouseEnabled(x=True, y=False)
            self.timeplot.setLimits(xMin=0, xMax=self.time.max())
            self.timeplot.autoRange()
            self.timeplot.showGrid(x=True)
            self.timeplot.setMenuEnabled(False)
            self.timeplot.setLabel('left', "EMG", "mV")
            self.timeplot.setLabel('bottom', "time", "s")

            self.timepoint = pg.CurvePoint(self.timeplot.curves[0])
            self.timeplot.addItem(self.timepoint)
            arrow = pg.ArrowItem(angle=-90)
            arrow.setParentItem(self.timepoint)

            self.fftplot.plot(self.time, self.signal, pen=pen)
            self.fftplot.setMouseEnabled(x=True, y=False)
            self.fftplot.showGrid(x=True)
            self.fftplot.setMenuEnabled(False)
            self.fftplot.setLabel('left', "Power", "")
            self.fftplot.setLabel('bottom', "frequency", "Hz")
            self.fftplot.curves[0].setFftMode(True)
            self.fftplot.enableAutoRange(True)

            self.elec_plot = pg.ScatterPlotItem()
            self.viewbox.addItem(self.elec_plot)
            #{‘pos’: (x, y), ‘size’, ‘pen’, ‘brush’, ‘symbol’}
            self.elec_plot.setData(x=[self.coords[grid_id][0].flatten()[0]], y=[self.coords[grid_id][1].flatten()[0]],
                                   symbol=['o'], size=20, color=(30,30,30,30))

        except KeyError as K_err:
            print(K_err)

    def update(self):
        elec_id = self.spin_elec.value()
        grid_id = self.combo_grid.currentIndex()
        idx = self.time_slider.value()
        self.timepoint.setPos(idx/self.signal.size)
        self.imgitem.setImage(self.cmap(self.image[idx]))
        self.signal = self.grids[grid_id]["signals"][elec_id]
        self.timeplot.curves[0].setData(x=self.time, y=self.signal)
        self.timeplot.autoRange()
        self.fftplot.curves[0].setData(x=self.time, y=self.signal)
        self.elec_plot.setData(x=[self.coords[grid_id][0].flatten()[elec_id]], y=[self.coords[grid_id][1].flatten()[elec_id]],
                               symbol=['o'], size=20, color=(30,30,30,30))


    def minimap(self, grid_id=None):
        if self.grids:
            if not grid_id:
                grid_id = self.combo_grid.currentIndex()
            shape = self.grids[grid_id]["tissue_shape"]
            centers_x = self.grids[grid_id]["centers"][1]
            centers_y = self.grids[grid_id]["centers"][0]
            background = numpy.zeros(shape)
            self.figmap = matplotlib.pyplot.figure("minimap", tight_layout=True)
            self.figmap.suptitle("Electrode positions",
                                 bbox=dict(boxstyle='round', facecolor='white', alpha=0.9, pad=0.5))
            cmaplist = matplotlib.colors.ListedColormap(
                [col['color'] for col in matplotlib.pyplot.rcParams['axes.prop_cycle']][:2])
            matplotlib.pyplot.imshow(background, origin='lower', cmap=cmaplist)
            for idx in range(centers_x.size):
                matplotlib.pyplot.text(centers_x.flatten()[idx], centers_y.flatten()[idx], "{}".format(idx),
                                       horizontalalignment='center', verticalalignment='center',
                                       bbox=dict(boxstyle='circle', facecolor='white', alpha=0.5, pad=0.5))
            self.figmap.show()


def main():
    app = QtGui.QApplication(sys.argv)
    ex = EMGExplorer()
    ex.showMaximized()
    ex.raise_()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
