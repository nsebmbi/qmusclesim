# -*- coding: utf-8 -*-
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import json
import time

import jsonschema
from PyQt4 import QtGui

# from PyQt4.QtGui import QDialog, QFileDialog, QTreeWidgetItem, QInputDialog, QErrorMessage
from PyQt4.QtCore import Qt

from .parameters import Ui_Dialog
from pkg_resources import resource_filename
from copy import deepcopy


def walk_dict_tree(mondict, parent):
    if isinstance(mondict, dict):
        if 'name' in mondict:
            if 'class' in mondict:
                leaf = QtGui.QTreeWidgetItem(parent, [mondict['name'], "Model", mondict['class']])
                walk_dict_tree(mondict['parameters'], leaf)
                if 'model' in mondict:
                    walk_dict_tree(mondict['model'], leaf)
            else:
                QtGui.QTreeWidgetItem(parent, [mondict['name'], mondict['unit'], repr(mondict['value'])])
        else:
            for key, val in mondict.iteritems():
                walk_dict_tree(val, parent)
    elif isinstance(mondict, list):
        for item in mondict:
            walk_dict_tree(item, parent)
    else:
        pass


def find_path(element, params_json, path, all_paths):
    try:
        if element in params_json:
            path = params_json[element].encode('utf-8') + '@' + path
            all_paths.update([[path.split('@')[0], path.split('@')[-1][:-1]]])
        for key in params_json:
            if isinstance(params_json[key], dict):
                find_path(element, params_json[key], path + key + '.', all_paths)
            elif isinstance(params_json[key], list):
                for idx, item in enumerate(params_json[key]):
                    find_path(element, item, path + key + ".{}.".format(idx), all_paths)
    except TypeError:
        pass


def get_all(myjson, key):
    """
    Yeild all *key* in the input json
    :param myjson: Json input object
    :param key: key to return
    :return: dict
    """
    if type(myjson) is dict:
        for jsonkey in myjson:
            if jsonkey == key:
                yield myjson[jsonkey]
            elif type(myjson[jsonkey]) in (list, dict):
                for d in get_all(myjson[jsonkey], key):
                    yield d
    elif type(myjson) is list:
        for item in myjson:
            if type(item) in (list, dict):
                for d in get_all(item, key):
                    yield d


class AddDialog(QtGui.QDialog, Ui_Dialog):
    """
    Add parameter dialog.
    Parameter name and unit are suggested from units dictionary.
    :param parent:
    """

    def __init__(self, parent=None):
        super(AddDialog, self).__init__(parent)
        self.setupUiadd(self)
        self.parent = parent
        self.comboBox_Nom.addItems(sorted(list(set(self.parent.wizard().units.keys()))))
        self.comboBox_Type.addItems(sorted(list(set(self.parent.wizard().units.values()))))
        self.comboBox_Type.setEditable(True)
        self.comboBox_Nom.setFocus()
        self.parItem = self.parent.treeview.currentItem()
        if self.parItem.text(1) != "Model":
            self.parItem = self.parItem.parent()

    def add_param(self):
        """
        Add parameter to tree from inputs in the dialog.

        """
        QtGui.QTreeWidgetItem(
            self.parItem,
            [self.comboBox_Nom.currentText().__str__(),
             self.comboBox_Type.currentText().__str__(), self.lineEdit_P1.text()])
        self.parent.treeview.expandAll()
        self.accept()


class ModifyDialog(QtGui.QDialog, Ui_Dialog):
    """
    Modify selected parameter dialog.
    :param parent:
    """

    def __init__(self, parent=None):
        super(ModifyDialog, self).__init__(parent)
        self.setupUimod(self)
        self.parent = parent
        self.comboBox_Type.addItems(sorted(list(set(self.parent.wizard().units.values()))))
        self.comboBox_Type.setEditable(True)

        self.citem = parent.treeview.currentItem()
        self.parItem = self.citem.parent()
        self.lbl_keyname = QtGui.QLabel(self.citem.text(0), self.formLayoutWidget)  # label de texte pour le name
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.lbl_keyname)
        self.comboBox_Type.setCurrentIndex(self.comboBox_Type.findText(self.citem.text(1), Qt.MatchFixedString))
        self.lineEdit_P1.setText(self.citem.text(2))

    def add_param(self):
        """
        Add modified parameter to the tree.

        """
        tree = self.parent.treeview
        self.citem.setText(1, self.comboBox_Type.currentText().__str__())
        self.citem.setText(2, self.lineEdit_P1.text())
        tree.expandAll()
        self.accept()


class PageParam(QtGui.QWizardPage):
    """ Second QWizard Page for initialization of model's paramaters"""

    def __init__(self, parent=None):

        super(PageParam, self).__init__(parent)

        self.setTitle("Edit model parameters.")
        self.setSubTitle("Load or create json description of the simulation to perform.")
        vbox = QtGui.QVBoxLayout()
        # Création des boutons
        buttonhbox = QtGui.QHBoxLayout()

        clear_button = QtGui.QPushButton('New', self)
        clear_button.clicked.connect(self.newtree)
        buttonhbox.addWidget(clear_button)
        open_button = QtGui.QPushButton('Open', self)
        open_button.clicked.connect(self.loadjson)
        buttonhbox.addWidget(open_button)
        save_button = QtGui.QPushButton('Save', self)
        save_button.clicked.connect(self.savejson)
        buttonhbox.addWidget(save_button)
        self.add_mdl_button = QtGui.QPushButton('Add Model', self)
        buttonhbox.addWidget(self.add_mdl_button)
        add_par_button = QtGui.QPushButton('Add Parameter', self)
        add_par_button.clicked.connect(self.showadddialog)
        buttonhbox.addWidget(add_par_button)
        modify_button = QtGui.QPushButton('Modify', self)
        modify_button.clicked.connect(self.showmoddialog)
        buttonhbox.addWidget(modify_button)
        delete_button = QtGui.QPushButton('Delete', self)
        delete_button.clicked.connect(self.delete_param)
        buttonhbox.addWidget(delete_button)

        vbox.addLayout(buttonhbox)

        # Déclarations
        self.tree = None
        self.root = None
        self.filename = None
        self.json = None
        self.m_helper = None
        self.errorMessageDialog = QtGui.QErrorMessage(self)

        # Déclaration de l'arbre
        self.treeview = QtGui.QTreeWidget(self)
        self.treeview.setHeaderLabels(('Param', 'Unit', 'Value'))
        self.treeview.header().setResizeMode(QtGui.QHeaderView.ResizeToContents)
        self.treeview.header().setStretchLastSection(False)
        vbox.addWidget(self.treeview)

        purposehbox = QtGui.QHBoxLayout()
        self.purposeLine = QtGui.QLineEdit(self)
        purposehbox.addWidget(QtGui.QLabel("Purpose: "))
        purposehbox.addWidget(self.purposeLine)
        vbox.addLayout(purposehbox)
        self.setLayout(vbox)

    def initializePage(self):
        #if self.wizard().data['model'].startswith("uemg"):
        #    self.m_helper = ParamUEMG(self)
        #else:
        self.m_helper = ParamNEW(self)
        self.add_mdl_button.clicked.connect(self.m_helper.showaddmdl)
        self.purposeLine.setText(self.wizard().data['purpose'])

    def newtree(self):
        """
        Clear the definition tree to start a new one.
        """
        self.treeview.clear()
        self.json = deepcopy(self.m_helper.json)
        trunk = QtGui.QTreeWidgetItem(self.treeview, ["Muscle", self.json['Muscle']['Kind']])

        walk_dict_tree(self.json, trunk)
        self.treeview.sortItems(0, Qt.AscendingOrder)
        self.treeview.expandAll()
        self.wizard().data['json'] = ""

    def loadjson(self, filename=None):
        """

        :param filename:
        """
        if not filename:
            self.filename = QtGui.QFileDialog.getOpenFileName(
                None,
                "Open Paremeters File",
                self.wizard().data['directory'],
                "JSON Files (*.json)")

        try:
            with open(self.filename, 'r') as fjson:
                self.json = json.load(fjson)
        except IOError as err:
            print("Failure opening JSON File")
            raise err
        if (self.wizard().data['model'].startswith("striat") and self.json['Muscle']['Kind'].lower() == "striated") or \
                (self.wizard().data['model'].startswith("semg") and self.json['Muscle']['Kind'].lower() == "striated") or (
                    self.wizard().data['model'].startswith("uemg") and self.json['Muscle'][
                    'Kind'].lower() == "uterine"):
            self.treeview.clear()
            if self.wizard().data['purpose'] != self.json['Purpose']:
                reply = QtGui.QMessageBox.question(self, 'Overwrite Purpose',
                                                   "Replace purpose:\n{}\nby:\n{}".format(self.wizard().data['purpose'],
                                                                                          self.json["Purpose"]),
                                                   QtGui.QMessageBox.Yes |
                                                   QtGui.QMessageBox.No, QtGui.QMessageBox.No)
                if reply == QtGui.QMessageBox.Yes:
                    self.wizard().data['purpose'] = self.json['Purpose']
                    self.purposeLine.setText(self.wizard().data['purpose'])
            trunk = QtGui.QTreeWidgetItem(
                self.treeview, ["Muscle", self.json['Muscle']['Kind']])

            walk_dict_tree(self.json, trunk)
            self.treeview.sortItems(0, Qt.AscendingOrder)
            self.treeview.expandAll()
        else:
            QtGui.QMessageBox.warning(None, "Wrong muscle in json",
                                      "File: {}\n for {} muscle can not be used to initialise a {} model.".format(
                                          str(self.filename), self.json['Muscle']['Kind'].lower(),
                                          self.wizard().data['model']))

    def savejson(self):
        self.json = dict()
        self.filename = str(QtGui.QFileDialog.getSaveFileName(
            self,
            "Save Paremeters",
            self.wizard().data['directory'],
            "JSON Files (*.json)"))
        self.update_json()

        with open(self.filename, "w") as fjson:
            json.dump(self.json, fjson, indent=4, sort_keys=True)
        self.loadjson(self.filename)
        self.wizard().data['json'] = self.filename

    def showmoddialog(self):
        """
        Show dialog to modify the selected parameter.
        """
        try:
            self.treeview.currentItem().parent().parent()
        except AttributeError:
            self.errorMessageDialog.showMessage("Select a parameter to modify.")
        else:
            mod = ModifyDialog(self)
            self.wizard().data['json'] = ""
            mod.show()

    def showadddialog(self):
        """
        Show full dialog to add a new parameter to the selected model.
        """
        try:
            self.treeview.currentItem().parent()
        except AttributeError:
            self.errorMessageDialog.showMessage("Select a model to add the parameter to.")
        else:
            add = AddDialog(self)
            self.wizard().data['json'] = ""
            add.show()

    def delete_param(self):
        """
        Delete the selected parameter or model.

        """
        self.wizard().data['json'] = ""
        root = self.treeview.invisibleRootItem()
        for item in self.treeview.selectedItems():
            (item.parent() or root).removeChild(item)
        self.update_json()
        trunk = self.treeview.topLevelItem(0)
        # trunk = QtGui.QTreeWidgetItem(self.treeview, ["Muscle", self.json['Muscle']['Kind']])

        walk_dict_tree(self.json, trunk)
        self.treeview.sortItems(0, Qt.AscendingOrder)
        self.treeview.expandAll()

    def update_json(self):
        self.json["Timestamp"] = time.strftime(
            "%d/%m/%Y %H:%M:%S", time.localtime())

        self.json["Purpose"] = str(self.wizard().data['purpose'])
        self.json["Muscle"] = dict(Kind=str(self.treeview.topLevelItem(0).text(1)), parameters=[], Models=[])
        mdls = []
        for child in self.treeview.topLevelItem(0).takeChildren():
            if str(child.text(1)) == "Model":
                mid = mdls.count(str(child.text(0))) + 1
                mdls.append(str(child.text(0)))
                self.json["Muscle"]["Models"].append(
                    {"name": str(child.text(0)), "class": str(child.text(2)), "parameters": [], "model": []})
                self.json["Muscle"]["Models"][-1]["id"] = mid
                smdls = []
                for schild in child.takeChildren():
                    if str(schild.text(1)) == "Model":
                        smid = smdls.count(str(schild.text(0))) + 1
                        smdls.append(str(schild.text(0)))
                        self.json["Muscle"]["Models"][-1]["model"].append(
                            {"name": str(schild.text(0)), "class": str(schild.text(2)), "parameters": []})
                        for sschild in schild.takeChildren():
                            self.json["Muscle"]["Models"][-1]["model"][-1]["parameters"].append(
                                {"name": str(sschild.text(0)), "unit": str(sschild.text(1)),
                                 "value": eval(str(sschild.text(2)))})
                        self.json["Muscle"]["Models"][-1]["model"][-1]["id"] = smid
                    else:
                        self.json["Muscle"]["Models"][-1]["parameters"].append(
                            {"name": str(schild.text(0)), "unit": str(schild.text(1)),
                             "value": eval(str(schild.text(2)))})
                if len(self.json["Muscle"]["Models"][-1]["model"]) == 0:
                    self.json["Muscle"]["Models"][-1].pop("model")
            else:
                self.json["Muscle"]["parameters"].append(
                    {"name": str(child.text(0)), "unit": str(child.text(1)), "value": eval(str(child.text(2)))})

    def validatePage(self):
        try:
            with open(resource_filename(self.wizard().data['model'], '/data/schema_muscle.json'), 'r') as f_schema:
                jsonschema.validate(self.json, json.load(f_schema))
        except jsonschema.exceptions.ValidationError as val_err:
            error_mess = str(val_err).splitlines()[:5]
            error_mess.extend(["", "...", "", "Please check your json's structure!"])
            QtGui.QMessageBox.warning(self, "Json doesn't pass validation", "\n".join(error_mess))
            return False
        if self.wizard().data['json'] == "":
            self.savejson()
            return False
        else:
            return True

    def show_dialog_check(self, err):

        text = QtGui.QMessageBox(self)
        text.open()
        text.setWindowTitle("Something's wrong!")
        # text.setGeometry(650, 350, 250, 1000)
        text.setText("Error {0}".format(err))


class ParamUEMG:
    """
    Model definition class for uemg_squared
    """

    def __init__(self, parent):
        """

        :return:
        """
        self.parent = parent
        with open(resource_filename(self.parent.wizard().data['model'], 'data/all_models.json'), 'r') as f_models:
            self.all_models = json.load(f_models)
        with open(resource_filename(self.parent.wizard().data['model'], 'data/skeleton.json'), 'r') as f_skel:
            self.skel = json.load(f_skel)
        self.json = deepcopy(self.skel)
        self.submodels = self.all_models.keys()

    def showaddmdl(self):
        """
        Show simple dialog to add a model from the possible ones.

        """
        mname = str(QtGui.QInputDialog.getItem(self.parent, "Add model",
                                               "Please select a model to add:", sorted(self.submodels), 0,
                                               False)[0])
        self.parent.json["Muscle"]["Models"].append(self.all_models[mname])
        self.parent.treeview.clear()
        trunk = QtGui.QTreeWidgetItem(
            self.parent.treeview, ["Muscle", self.json['Muscle']['Kind']])
        walk_dict_tree(self.parent.json, trunk)

        self.parent.treeview.sortItems(0, Qt.AscendingOrder)
        self.parent.wizard().data['json'] = ""
        self.parent.treeview.expandAll()


class ParamNEW:
    """
    Model definition class for uemg_squared
    """

    def __init__(self, parent):
        """

        :return:
        """
        self.parent = parent
        with open(resource_filename(self.parent.wizard().data['model'], 'data/all_models.json'), 'r') as f_models:
            self.all_models = json.load(f_models)
        with open(resource_filename(self.parent.wizard().data['model'], 'data/skeleton.json'), 'r') as f_skel:
            self.skel = json.load(f_skel)
        self.json = deepcopy(self.skel)
        self.submodels = self.all_models.keys()

    def showaddmdl(self):
        """
        Show simple dialog to add a model from the possible ones.

        """
        mname = str(QtGui.QInputDialog.getItem(self.parent, "Add model",
                                               "Please select a model to add:", sorted(self.submodels), 0,
                                               False)[0])

        if len(self.all_models[mname]["dependencies"]) == 0:
            self.parent.json["Muscle"]["Models"].append(self.all_models[mname]["stub"])
        else:
            class_paths = dict()
            find_path('class', self.parent.json, '', class_paths)
            valid_dep = [depend for depend in self.all_models[mname]["dependencies"] if
                         depend in class_paths.keys()]

            if not valid_dep:
                self.parent.errorMessageDialog.showMessage("Please first add one of the following dependencies: {}.".format(
                    self.all_models[mname]["dependencies"]))
            elif len(valid_dep) > 1:
                self.parent.errorMessageDialog.showMessage(
                    "Dependencies: {} are mutually exclusive, please keep only one of them.".format(
                        valid_dep))
            else:
                insert_path = class_paths[valid_dep[0]].split('.')
                if eval(self.all_models[mname]["inclusion"]):
                    insert_path.append('model')
                else:
                    insert_path.pop()

                insert_point = self.parent.json
                for key in insert_path:
                    if key.isdigit():
                        key = int(key)
                    insert_point = insert_point[key]
                insert_point.append(self.all_models[mname]["stub"])

        self.parent.treeview.clear()
        trunk = QtGui.QTreeWidgetItem(
            self.parent.treeview, ["Muscle", self.json['Muscle']['Kind']])
        walk_dict_tree(self.parent.json, trunk)

        self.parent.treeview.sortItems(0, Qt.AscendingOrder)
        self.parent.wizard().data['json'] = ""
        self.parent.treeview.expandAll()
